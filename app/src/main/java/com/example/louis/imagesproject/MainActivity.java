package com.example.louis.imagesproject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.renderscript.Allocation;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.rssample.ScriptC_grey;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Bitmap bm;
    ImageView image_current;
    Button button_reset, button_grey, button_red, button_hue, button_contrast_ng_1, button_contrast_ng_2, button_contrast_rgb_1, button_contrast_rgb_2, button_blurring, button_grey_rs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        image_current = findViewById(R.id.imageCurrent);
        bm = BitmapFactory.decodeResource(getResources(), R.drawable.fruits);
        bm = bm.copy(Bitmap.Config.ARGB_8888, true);
        image_current.setImageBitmap(bm);

        button_reset = findViewById(R.id.buttonReset);
        button_grey = findViewById(R.id.buttonGrey);
        button_grey_rs = findViewById(R.id.buttonGreyRs);
        button_red = findViewById(R.id.buttonRed);
        button_hue = findViewById(R.id.buttonHue);
        button_contrast_ng_1 = findViewById(R.id.buttonContrastNg1);
        button_contrast_ng_2 = findViewById(R.id.buttonContrastNg2);
        button_contrast_rgb_1 = findViewById(R.id.buttonContrastRgb1);
        button_contrast_rgb_2 = findViewById(R.id.buttonContrastRgb2);
        button_blurring = findViewById(R.id.buttonBlurring);

        button_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bm = BitmapFactory.decodeResource(getResources(), R.drawable.fruits);
                bm = bm.copy(Bitmap.Config.ARGB_8888, true);
                image_current.setImageBitmap(bm);
            }
        });

        button_grey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                to_gray(bm);
            }
        });

        button_grey_rs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                renderscript_gray(bm);
            }
        });

        button_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                red_only(bm);
            }
        });

        button_hue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorize(bm);
            }
        });

        button_contrast_ng_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contrast_ng(bm);
            }
        });

        button_contrast_rgb_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contrast_color(bm);
            }
        });

        button_contrast_ng_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contrast_equalization_ng(bm);
            }
        });

        button_contrast_rgb_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contrast_equalization_color(bm);
            }
        });

        button_blurring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blurring(bm);
            }
        });

    }

    void renderscript_gray(Bitmap bitmap){
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bitmap);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_grey greyScript = new ScriptC_grey(rs);

        greyScript.forEach_toGrey(input, output);

        output.copyTo(bitmap);

        input.destroy();
        output.destroy();
        greyScript.destroy();
        rs.destroy();

        image_current.setImageBitmap(bitmap);
    }

    void toGray_slow(Bitmap bmp) {
        for (int x = 0; x < bmp.getWidth(); x++) {
            for (int y = 0; y < bmp.getHeight(); y++) {
                int px = bmp.getPixel(x, y);
                int r = Color.red(px);
                int g = Color.green(px);
                int b = Color.blue(px);
                r = g = b = (int) (0.3 * r + 0.59 * g + 0.11 * b);
                bmp.setPixel(x, y, Color.rgb(r, g, b));
            }
        }
    }

    void to_gray(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int tab[] = new int[bmp.getWidth() * bmp.getHeight()];
        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            int r = Color.red(pixels[i]);
            int g = Color.green(pixels[i]);
            int b = Color.blue(pixels[i]);
            r = g = b = (int) (0.3 * r + 0.59 * g + 0.11 * b);

            tab[i] = Color.rgb(r, g, b);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    void red_only(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int tab[] = new int[bmp.getWidth() * bmp.getHeight()];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            float[] hsv = new float[3];
            Color.colorToHSV(pixels[i], hsv);
            if (hsv[0] <= 20 || hsv[0] >= 340)
                tab[i] = pixels[i];
            else {
                int r = Color.red(pixels[i]);
                int g = Color.green(pixels[i]);
                int b = Color.blue(pixels[i]);
                r = g = b = (int) (0.3 * r + 0.59 * g + 0.11 * b);
                tab[i] = Color.rgb(r, g, b);
            }
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    void colorize(Bitmap bmp) {
        Random r = new Random();
        int color = r.nextInt(360);

        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int tab[] = new int[bmp.getWidth() * bmp.getHeight()];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            float[] hsv = new float[3];
            Color.colorToHSV(pixels[i], hsv);
            hsv[0] = color;
            tab[i] = Color.HSVToColor(3, hsv);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);

    }

    void contrast_ng(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);

        int tab[] = new int[bmp.getWidth() * bmp.getHeight()];
        int[] hist = histogram_ng(pixels, bmp);
        int[] lut = new int[256];


        int cpt, min, max;
        cpt = 0;

        while (hist[cpt] == 0) {
            cpt++;
        }
        min = cpt;
        cpt = hist.length - 1;

        while (hist[cpt] == 0) {
            cpt--;
        }
        max = cpt;

        for (int i = 0; i < 256; i++)
            lut[i] = ((255 * (i - min)) / (max - min));


        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            int pixel = lut[(int) (0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]))];
            tab[i] = Color.rgb(pixel, pixel, pixel);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    int[] histogram_ng(int[] pixels, Bitmap bmp) {
        int hist[] = new int[256];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            hist[(int) (0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]))]++;
        }

        return hist;
    }

    void contrast_color(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);

        int tab[] = new int[bmp.getWidth() * bmp.getHeight()];
        int[][] hist = histogram_color(pixels, bmp);
        int[][] lut = new int[256][3];
        int cpt, min, max;

        for (int i = 0; i < 3; i++) {
            cpt = 0;

            while (hist[cpt][i] == 0) {
                cpt++;
            }
            min = cpt;
            cpt = hist.length - 1;

            while (hist[cpt][i] == 0) {
                cpt--;
            }
            max = cpt;

            for (int j = 0; j < 256; j++)
                lut[j][i] = ((255 * (j - min)) / (max - min));
        }

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            int r = lut[Color.red(pixels[i])][0];
            int g = lut[Color.green(pixels[i])][1];
            int b = lut[Color.blue(pixels[i])][2];

            tab[i] = Color.rgb(r, g, b);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    int[][] histogram_color(int[] pixels, Bitmap bmp) {
        int hist[][] = new int[256][3];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            hist[Color.red(pixels[i])][0]++;
            hist[Color.green(pixels[i])][1]++;
            hist[Color.blue(pixels[i])][2]++;
        }

        return hist;
    }

    void contrast_equalization_ng(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int[] cumul_hist = cumulate_histogram_ng(pixels, bmp);
        int[] tab = new int[bmp.getWidth() * bmp.getHeight()];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            int pixel = (cumul_hist[(int) (0.3 * Color.red(pixels[i]) + 0.59 * Color.green(pixels[i]) + 0.11 * Color.blue(pixels[i]))] * 255) / (bmp.getWidth() * bmp.getHeight());
            tab[i] = Color.rgb(pixel, pixel, pixel);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    int[] cumulate_histogram_ng(int[] pixels, Bitmap bmp) {
        int[] cumul_hist = new int[256];
        int[] tab;
        int nb = 0;
        tab = histogram_ng(pixels, bmp);

        for (int i = 0; i < 256; i++) {
            nb += tab[i];
            cumul_hist[i] = nb;
        }

        return cumul_hist;
    }

    void contrast_equalization_color(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int[][] cumul_hist = cumulate_histogram_color(pixels, bmp);
        int[] tab = new int[bmp.getWidth() * bmp.getHeight()];

        for (int i = 0; i < (bmp.getWidth() * bmp.getHeight()) - 1; i++) {
            int r = ((cumul_hist[Color.red(pixels[i])][0]) * 255) / (bmp.getWidth() * bmp.getHeight());
            int g = ((cumul_hist[Color.green(pixels[i])][1]) * 255) / (bmp.getWidth() * bmp.getHeight());
            int b = ((cumul_hist[Color.blue(pixels[i])][2]) * 255) / (bmp.getWidth() * bmp.getHeight());

            tab[i] = Color.rgb(r, g, b);
        }

        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }

    int[][] cumulate_histogram_color(int[] pixels, Bitmap bmp) {
        int[][] cumul_hist = new int[256][3];
        int[][] tab;
        int nb_r = 0;
        int nb_g = 0;
        int nb_b = 0;
        tab = histogram_color(pixels, bmp);

        for (int i = 0; i < 256; i++) {
            nb_r += tab[i][0];
            nb_g += tab[i][1];
            nb_b += tab[i][2];

            cumul_hist[i][0] = nb_r;
            cumul_hist[i][1] = nb_g;
            cumul_hist[i][2] = nb_b;
        }

        return cumul_hist;
    }

    void blurring(Bitmap bmp) {
        int pixels[] = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
        int[] tab = new int[bmp.getWidth() * bmp.getHeight()];
        int[] pixel = new int[3];

        for(int l = 0; l < bmp.getHeight(); l++){
            for(int c = 0; c < bmp.getWidth(); c++){
                if(l >= 1 && l < bmp.getHeight() - 1 && c > 1 && c < bmp.getWidth() - 1){
                    for(int i = 0; i < 3; i++){
                        for(int j = -1; j < 2; j++){
                            switch (i){
                                case 0 :
                                    pixel[i] += Color.red(pixels[(l + j) * bmp.getWidth() + c - 1]);
                                    pixel[i] += Color.red(pixels[(l + j) * bmp.getWidth() + c]);
                                    pixel[i] += Color.red(pixels[(l + j) * bmp.getWidth() + c + 1]);
                                    break;
                                case 1:
                                    pixel[i] += Color.green(pixels[(l + j) * bmp.getWidth() + c - 1]);
                                    pixel[i] += Color.green(pixels[(l + j) * bmp.getWidth() + c]);
                                    pixel[i] += Color.green(pixels[(l + j) * bmp.getWidth() + c + 1]);
                                    break;
                                case 2:
                                    pixel[i] += Color.blue(pixels[(l + j) * bmp.getWidth() + c - 1]);
                                    pixel[i] += Color.blue(pixels[(l + j) * bmp.getWidth() + c]);
                                    pixel[i] += Color.blue(pixels[(l + j) * bmp.getWidth() + c + 1]);
                                    break;
                            }
                        }
                        pixel[i] = pixel[i]/9;
                    }
                }
                else {
                    pixel[0] =  Color.red(pixels[l * bmp.getWidth() + c]);
                    pixel[1] =  Color.green(pixels[l * bmp.getWidth() + c]);
                    pixel[2] =  Color.blue(pixels[l * bmp.getWidth() + c]);
                }
                tab[l * bmp.getWidth() + c] = Color.rgb(pixel[0], pixel[1], pixel[2]);
            }
        }
        bmp.setPixels(tab, 1, bmp.getWidth(), 0, 0, bmp.getWidth() - 1, bmp.getHeight() - 1);
    }



}
